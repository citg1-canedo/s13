package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    public void createPost(Post post) {

        postRepository.save(post);
    }

    //    Get All Posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //    Delete post
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
    }

    //    Update post
    public ResponseEntity updatePost(Long id, Post post){
//        Find the post to update
        Post postForUpdate = postRepository.findById(id).get();

//        Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

//        Saving and updating a post
        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
    }


}
